#pragma once

#include <array>
#include <tuple>
// https://stackoverflow.com/a/45287719


template <class Target=void, class... TupleLike>
constexpr auto array_concat(TupleLike&&... tuples) {
    return std::apply([](auto&& first, auto&&... rest){
        using T = std::conditional_t<
            !std::is_void<Target>::value, Target, std::decay_t<decltype(first)>>;
        return std::array<T, sizeof...(rest)+1>{{
            decltype(first)(first), decltype(rest)(rest)...
        }};
    }, std::tuple_cat(std::forward<TupleLike>(tuples)...));
}
