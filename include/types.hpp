#pragma once
#include <cstdint>
#include <map>
#include <string>
#include <tuple>
#include <vector>
namespace nakanawa {
using char_t = uint32_t;
using range_t = std::pair<char_t, char_t>;
using tree_source_node = std::pair<char_t, std::string>;
using tree_source = std::map<char_t, std::vector<tree_source_node>>;
} // namespace nakanawa