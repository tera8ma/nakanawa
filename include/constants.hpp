#pragma once

#include "helpers.hpp"
#include "types.hpp"

#include <array>
#include <tuple>

namespace nakanawa::constants {

constexpr const char_t LATIN_LOWERCASE_START = 0x61;
constexpr const char_t LATIN_LOWERCASE_END = 0x7a;
constexpr const char_t LATIN_UPPERCASE_START = 0x41;
constexpr const char_t LATIN_UPPERCASE_END = 0x5a;
constexpr const char_t LOWERCASE_ZENKAKU_START = 0xff41;
constexpr const char_t LOWERCASE_ZENKAKU_END = 0xff5a;
constexpr const char_t UPPERCASE_ZENKAKU_START = 0xff21;
constexpr const char_t UPPERCASE_ZENKAKU_END = 0xff3a;
constexpr const char_t HIRAGANA_START = 0x3041;
constexpr const char_t HIRAGANA_END = 0x3096;
constexpr const char_t KATAKANA_START = 0x30a1;
constexpr const char_t KATAKANA_END = 0x30fc;
constexpr const char_t KANJI_START = 0x4e00;
constexpr const char_t KANJI_END = 0x9faf;
constexpr const char_t PROLONGED_SOUND_MARK = 0x30fc;
constexpr const char_t KANA_SLASH_DOT = 0x30fb;

constexpr const range_t ZENKAKU_NUMBERS = {0xff10, 0xff19};
constexpr const range_t ZENKAKU_UPPERCASE = {UPPERCASE_ZENKAKU_START,
                                             UPPERCASE_ZENKAKU_END};
constexpr const range_t ZENKAKU_LOWERCASE = {LOWERCASE_ZENKAKU_START,
                                             LOWERCASE_ZENKAKU_END};
constexpr const range_t ZENKAKU_PUNCTUATION_1 = {0xff01, 0xff0f};
constexpr const range_t ZENKAKU_PUNCTUATION_2 = {0xff1a, 0xff1f};
constexpr const range_t ZENKAKU_PUNCTUATION_3 = {0xff3b, 0xff3f};
constexpr const range_t ZENKAKU_PUNCTUATION_4 = {0xff5b, 0xff60};
constexpr const range_t ZENKAKU_SYMBOLS_CURRENCY = {0xffe0, 0xffee};

constexpr const range_t HIRAGANA_CHARS = {0x3040, 0x309f};
constexpr const range_t KATAKANA_CHARS = {0x30a0, 0x30ff};
constexpr const range_t HANKAKU_KATAKANA = {0xff66, 0xff9f};
constexpr const range_t KATAKANA_PUNCTUATION = {0x30fb, 0x30fc};
constexpr const range_t KANA_PUNCTUATION = {0xff61, 0xff65};
constexpr const range_t CJK_SYMBOLS_PUNCTUATION = {0x3000, 0x303f};
constexpr const range_t COMMON_CJK = {0x4e00, 0x9fff};
constexpr const range_t RARE_CJK = {0x3400, 0x4dbf};

constexpr const std::array KANA_RANGES = {HIRAGANA_CHARS, KATAKANA_CHARS,
                                          KANA_PUNCTUATION, HANKAKU_KATAKANA};

constexpr const std::array JA_PUNCTUATION_RANGES = {
    CJK_SYMBOLS_PUNCTUATION, KANA_PUNCTUATION,        KATAKANA_PUNCTUATION,
    ZENKAKU_PUNCTUATION_1,   ZENKAKU_PUNCTUATION_2,   ZENKAKU_PUNCTUATION_3,
    ZENKAKU_PUNCTUATION_4,   ZENKAKU_SYMBOLS_CURRENCY};

// All Japanese unicode start and end ranges
// Includes kanji, kana, zenkaku latin chars, punctuation, and number ranges.
constexpr const std::array JAPANESE_RANGES =
    array_concat(KANA_RANGES, JA_PUNCTUATION_RANGES,
                 std::make_tuple(ZENKAKU_UPPERCASE, ZENKAKU_LOWERCASE,
                                 ZENKAKU_NUMBERS, COMMON_CJK, RARE_CJK));

constexpr const range_t MODERN_ENGLISH = {0x0000, 0x007f};
constexpr const std::array<range_t, 5> HEPBURN_MACRON_RANGES = {{
    {0x0100, 0x0101}, // Ā ā
    {0x0112, 0x0113}, // Ē ē
    {0x012a, 0x012b}, // Ī ī
    {0x014c, 0x014d}, // Ō ō
    {0x016a, 0x016b}, // Ū ū
}};

constexpr const std::array<range_t, 2> SMART_QUOTE_RANGES = {{
    {0x2018, 0x2019}, // ‘ ’
    {0x201c, 0x201d}, // “ ”
}};

constexpr const std::array ROMAJI_RANGES =
    array_concat(std::make_tuple(MODERN_ENGLISH), HEPBURN_MACRON_RANGES);

constexpr const std::array EN_PUNCTUATION_RANGES =
    array_concat(SMART_QUOTE_RANGES,
                 std::make_tuple(std::make_pair<char_t, char_t>(0x20, 0x2f),
                                 std::make_pair<char_t, char_t>(0x3a, 0x3f),
                                 std::make_pair<char_t, char_t>(0x5b, 0x60),
                                 std::make_pair<char_t, char_t>(0x7b, 0x7e)));
} // namespace nakanawa::constants