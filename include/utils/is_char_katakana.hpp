#pragma once

#include "constants.hpp"
#include "is_char_in_range.hpp"
#include "types.hpp"

namespace nakanawa::utils {
constexpr inline bool is_char_katakana(nakanawa::char_t c) {
  return is_char_in_range(c, constants::KATAKANA_START,
                          constants::KATAKANA_END);
}
} // namespace nakanawa::utils
