#pragma once

#include "tree/node.hpp"
#include "types.hpp"

namespace nakanawa::utils {
Node create_romaji_to_kana_map();
Node create_romaji_ime_map();

const Node &get_romanji_ime_map();

const Node &get_romaji_map();
} // namespace nakanawa::utils