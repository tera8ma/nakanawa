#pragma once

#include "constants.hpp"
#include "types.hpp"

namespace nakanawa::utils {
constexpr inline bool is_char_slash_dot(nakanawa::char_t c) {
  return constants::KANA_SLASH_DOT == c;
}
} // namespace nakanawa::utils
