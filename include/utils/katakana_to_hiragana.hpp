#pragma once
#include <string>
namespace nakanawa::utils {

std::string katakana_to_hiragana(const std::string &input);
} // namespace nakanawa::utils