#pragma once

#include "constants.hpp"
#include "is_char_in_range.hpp"
#include "is_char_long_dash.hpp"

namespace nakanawa::utils {
constexpr inline bool is_char_hiragana(nakanawa::char_t c) {
  return is_char_long_dash(c) || is_char_in_range(c, constants::HIRAGANA_START,
                                                  constants::HIRAGANA_END);
}
} // namespace nakanawa::utils