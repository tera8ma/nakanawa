#pragma once
#include <string>
namespace nakanawa::utils {
std::string hiragana_to_katakana(std::string::const_iterator begin,
                                 std::string::const_iterator end);
}