#pragma once

#include "tree/node.hpp"
#include "types.hpp"
#include <string>
#include <utf8.h>

namespace nakanawa::utils {
struct Chunk {
  Chunk(std::string::const_iterator _begin, std::string::const_iterator _end,
        const std::string &value)
      : begin{std::move(_begin)}, end{std::move(_end)},
        value_begin{value.cbegin()}, value_end{value.cend()} {}

  Chunk(std::string::const_iterator _begin, std::string::const_iterator _end,
        std::string::const_iterator _value_begin,
        std::string::const_iterator _value_end)
      : begin{std::move(_begin)}, end{std::move(_end)},
        value_begin{std::move(_value_begin)}, value_end{std::move(_value_end)} {
  }

  std::string::const_iterator begin;
  std::string::const_iterator end;
  std::string::const_iterator value_begin;
  std::string::const_iterator value_end;
};

Node transform(const tree_source &map);

Chunk new_chunk(const Node &root, std::string::const_iterator begin,
                std::string::const_iterator end, bool ime_mode);
} // namespace nakanawa::utils
