#pragma once

#include "constants.hpp"

namespace nakanawa::utils {
constexpr inline bool is_char_long_dash(nakanawa::char_t c) {
  return c == nakanawa::constants::PROLONGED_SOUND_MARK;
}
} // namespace nakanawa::utils