#pragma once

#include "types.hpp"

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>

namespace nakanawa {
class Node {
public:
  explicit Node(std::string value = "") : data{std::move(value)} {}

  inline auto find(nakanawa::char_t c) {
    return std::ranges::find_if(children,
                                [&c](const auto &p) { return p.first == c; });
  }

  const inline auto find(nakanawa::char_t c) const {
    return std::ranges::find_if(children,
                                [&c](const auto &p) { return p.first == c; });
  }
  Node &add_child(nakanawa::char_t c, std::string value = "") {
    children.emplace_back(std::move(c), std::move(value));
    return children.back().second;
  }

  Node &add_child(const std::string &path, const std::string &value);
  Node &find_or_create(const std::string &path);
  inline Node &find_or_create(nakanawa::char_t c) {
    auto it = find(c);
    if (it == children.end()) {
      return add_child(c);
    }
    return it->second;
  }

  void add_children(const std::vector<nakanawa::tree_source_node> &children) {
    for (const auto &child : children) {
      add_child(child.first, child.second);
    }
  }

  inline auto &get_children() { return children; }

  inline void set_data(const std::string &value) { data = value; }

  inline const std::string &get_data() const { return data; }

  inline const auto end() const { return children.end(); }

  inline Node &operator[](nakanawa::char_t c) { return find(c)->second; }

  inline const Node &operator[](nakanawa::char_t c) const {
    return find(c)->second;
  }

  const Node &traverse(const std::string &path) const {
    const Node *current = this;
    for (auto it = path.begin(); it != path.end(); it++) {
      current = &(current->find(*it)->second);
    }

    return *current;
  }

  inline bool is_leaf() const { return children.empty(); }

  inline bool is_empty() const { return data.empty(); }

private:
  std::string data;
  std::vector<std::pair<nakanawa::char_t, Node>> children;
};
} // namespace nakanawa
