#pragma once

#include "constants.hpp"
#include "utils/is_char_in_range.hpp"

namespace nakanawa::utils {
constexpr inline bool is_char_uppercase(nakanawa::char_t c) {
  return is_char_in_range(c, constants::LATIN_UPPERCASE_START,
                          constants::LATIN_UPPERCASE_END);
}
} // namespace nakanawa::utils