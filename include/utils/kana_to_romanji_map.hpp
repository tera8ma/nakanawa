#pragma once

#include "utils/tree/node.hpp"

namespace nakanawa::utils {
Node create_kana_to_romanji_map();
const Node &get_kana_to_romanji_map();
} // namespace nakanawa::utils
