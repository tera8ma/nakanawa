#pragma once

#include "is_char_in_range.hpp"
#include "types.hpp"

#include <algorithm>

namespace nakanawa::utils {
constexpr inline bool is_char_japanese_punctuation(nakanawa::char_t c) {
  return std::ranges::any_of(nakanawa::constants::JA_PUNCTUATION_RANGES,
                             [&c](const nakanawa::range_t &range) {
                               return is_char_in_range(c, range);
                             });
}
} // namespace nakanawa::utils