#pragma once

#include "types.hpp"

namespace nakanawa::utils {
constexpr inline bool is_char_in_range(nakanawa::char_t c,
                                       nakanawa::char_t start,
                                       nakanawa::char_t end) {
  return start <= c && c <= end;
}
constexpr inline bool is_char_in_range(nakanawa::char_t c,
                                       const nakanawa::range_t &range) {
  return std::get<0>(range) <= c && c <= std::get<1>(range);
}
} // namespace nakanawa::utils