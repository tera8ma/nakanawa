#pragma once

#include "utils/romanji_to_kana_map.hpp"
#include "utils/tree/node.hpp"
#include <string>

namespace nakanawa {

class RomanjiConverter {
public:
  explicit RomanjiConverter(bool ime_support)
      : root{ime_support ? utils::get_romanji_ime_map()
                         : utils::get_romaji_map()},
        ime_support{ime_support} {}
  RomanjiConverter(const Node &node, bool ime_support)
      : root{node}, ime_support{ime_support} {}

  std::string convert(const std::string &s, bool force_katakana = false) const;

private:
  void append(std::back_insert_iterator<std::string> &inserter,
              std::string::const_iterator begin,
              std::string::const_iterator end) const;

private:
  const Node &root;
  bool ime_support;
};
} // namespace nakanawa