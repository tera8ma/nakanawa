#pragma once

#include "utils/kana_to_romanji_map.hpp"
#include "utils/tree/node.hpp"
#include <string>

namespace nakanawa {
class KanaConverter {
public:
  KanaConverter() : root{utils::get_kana_to_romanji_map()} {}
  explicit KanaConverter(const Node &node) : root{node} {}

  std::string convert(const std::string &s, bool upcase_katakana = false) const;

private:
  const nakanawa::Node &root;
};
} // namespace nakanawa