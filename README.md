# nakanawa

couple of C++ files that translate hiragana, katakana, romanji. It's a port of core functionality of [WanaKana](https://github.com/WaniKani/WanaKana).

## Usage

Test files cover basic usage:

- [romanji to kana](test/test_romanji_to_kana.cpp)
- [hiragana or katakana to romanji](test/test_kana_to_romanji.cpp)
