#include "romanji_converter.hpp"
#include "utils/hiragana_to_katakana.hpp"
#include "utils/is_char_uppercase.hpp"
#include "utils/kana_mapping.hpp"
#include <algorithm>
#include <utf8.h>

namespace nakanawa {

void RomanjiConverter::append(std::back_insert_iterator<std::string> &inserter,
                              std::string::const_iterator begin,
                              std::string::const_iterator end) const {
  while (begin != end) {
    auto c = utf8::next(begin, end);
    utf8::append(c, inserter);
  }
}

std::string RomanjiConverter::convert(const std::string &s,
                                      bool force_katakana) const {
  std::string::const_iterator begin = s.cbegin();
  std::string::const_iterator end = s.cend();

  std::string result;
  result.reserve(s.size());
  auto inserter = std::back_inserter(result);

  while (begin != end) {
    auto chunk = utils::new_chunk(root, begin, end, ime_support);

    if (force_katakana ||
        std::all_of(chunk.begin, chunk.end, utils::is_char_uppercase)) {
      const std::string katakana =
          utils::hiragana_to_katakana(chunk.value_begin, chunk.value_end);
      append(inserter, katakana.cbegin(), katakana.cend());
    } else {
      append(inserter, chunk.value_begin, chunk.value_end);
    }
    begin = chunk.end;
  }

  return result;
}
} // namespace nakanawa