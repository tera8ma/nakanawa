#include "kana_converter.hpp"
#include "utils/is_char_katakana.hpp"
#include "utils/kana_mapping.hpp"
#include "utils/katakana_to_hiragana.hpp"
#include <algorithm>
#include <cctype>
#include <utf8.h>

namespace nakanawa {
namespace helper {
bool all_chars_katakana(auto it, auto end) {
  bool result = true;
  while (result && it != end) {
    auto c = utf8::next(it, end);
    result = utils::is_char_katakana(c);
  }
  return result;
}

inline auto get_original_it(const std::string &original,
                            const std::string &hiragana, auto it) {
  return original.cbegin() + std::distance(hiragana.cbegin(), it);
}
} // namespace helper

std::string KanaConverter::convert(const std::string &s,
                                   bool upcase_katakana) const {
  std::string result;
  result.reserve(s.size());
  auto inserter = std::back_inserter(result);

  std::string hiragana_only = utils::katakana_to_hiragana(s);
  std::string::const_iterator begin = hiragana_only.cbegin();
  std::string::const_iterator end = hiragana_only.cend();

  while (begin != end) {
    auto chunk = utils::new_chunk(root, begin, end, false);

    result.append(chunk.value_begin, chunk.value_end);

    if (upcase_katakana &&
        helper::all_chars_katakana(
            helper::get_original_it(s, hiragana_only, chunk.begin),
            helper::get_original_it(s, hiragana_only, chunk.end))) {
      auto position =
          result.size() - std::distance(chunk.value_begin, chunk.value_end);

      auto new_part = result.begin() + position;
      std::transform(new_part, result.end(), new_part,
                     [](char c) { return std::toupper(c); });
    }
    begin = chunk.end;
  }

  return result;
}
} // namespace nakanawa
