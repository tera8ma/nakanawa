#include "utils/kana_mapping.hpp"
#include "utils/tree/node.hpp"
#include <algorithm>
#include <bits/ranges_algo.h>
#include <string>
#include <tuple>
#include <vector>

namespace nakanawa::utils {
const nakanawa::tree_source BASIC_ROMAJI = {
    {'\0', {{L'あ', "a"},   {L'い', "i"},   {L'う', "u"},  {L'え', "e"},
            {L'お', "o"},   {L'か', "ka"},  {L'き', "ki"}, {L'く', "ku"},
            {L'け', "ke"},  {L'こ', "ko"},  {L'さ', "sa"}, {L'し', "shi"},
            {L'す', "su"},  {L'せ', "se"},  {L'そ', "so"}, {L'た', "ta"},
            {L'ち', "chi"}, {L'つ', "tsu"}, {L'て', "te"}, {L'と', "to"},
            {L'な', "na"},  {L'に', "ni"},  {L'ぬ', "nu"}, {L'ね', "ne"},
            {L'の', "no"},  {L'は', "ha"},  {L'ひ', "hi"}, {L'ふ', "fu"},
            {L'へ', "he"},  {L'ほ', "ho"},  {L'ま', "ma"}, {L'み', "mi"},
            {L'む', "mu"},  {L'め', "me"},  {L'も', "mo"}, {L'ら', "ra"},
            {L'り', "ri"},  {L'る', "ru"},  {L'れ', "re"}, {L'ろ', "ro"},
            {L'や', "ya"},  {L'ゆ', "yu"},  {L'よ', "yo"}, {L'わ', "wa"},
            {L'ゐ', "wi"},  {L'ゑ', "we"},  {L'を', "wo"}, {L'ん', "n"},
            {L'が', "ga"},  {L'ぎ', "gi"},  {L'ぐ', "gu"}, {L'げ', "ge"},
            {L'ご', "go"},  {L'ざ', "za"},  {L'じ', "ji"}, {L'ず', "zu"},
            {L'ぜ', "ze"},  {L'ぞ', "zo"},  {L'だ', "da"}, {L'ぢ', "ji"},
            {L'づ', "zu"},  {L'で', "de"},  {L'ど', "do"}, {L'ば', "ba"},
            {L'び', "bi"},  {L'ぶ', "bu"},  {L'べ', "be"}, {L'ぼ', "bo"},
            {L'ぱ', "pa"},  {L'ぴ', "pi"},  {L'ぷ', "pu"}, {L'ぺ', "pe"},
            {L'ぽ', "po"},  {L'ゔ', "vu"}}},
    {L'ゔ', {{L'ぁ', "va"}, {L'ぃ', "vi"}, {L'ぇ', "ve"}, {L'ぉ', "vo"}}}};

const std::vector<std::pair<nakanawa::char_t, std::string>> SPECIAL_SYMBOLS = {
    {L'。', "."}, {L'、', ","}, {L'：', ":"}, {L'・', "/"}, {L'！', "!"},
    {L'？', "?"}, {L'〜', "~"}, {L'ー', "-"}, {L'「', "‘"}, {L'」', "’"},
    {L'『', "“"}, {L'』', "”"}, {L'［', "["}, {L'］', "]"}, {L'（', "("},
    {L'）', ")"}, {L'｛', "{"}, {L'｝', "}"}, {L'　', " "},
};

const std::vector<std::pair<nakanawa::char_t, std::string>> SMALL_Y = {
    {L'ゃ', "ya"}, {L'ゅ', "yu"}, {L'ょ', "yo"}};

const std::vector<std::pair<nakanawa::char_t, std::string>> SMALL_Y_EXTRA = {
    {L'ぃ', "yi"}, {L'ぇ', "ye"}};

const std::vector<std::pair<nakanawa::char_t, std::string>> SMALL_AIUEO = {
    {L'ぁ', "a"}, {L'ぃ', "i"}, {L'ぅ', "u"}, {L'ぇ', "e"}, {L'ぉ', "o"},
};

const std::vector<nakanawa::char_t> YOON_KANA = {L'き', L'に', L'ひ', L'み',
                                                 L'り', L'ぎ', L'び', L'ぴ',
                                                 L'ゔ', L'く', L'ふ'};

const std::vector<std::pair<nakanawa::char_t, std::string>> YOON_EXCEPTIONS = {
    {L'し', "sh"}, {L'ち', "ch"}, {L'じ', "j"}, {L'ぢ', "j"}};

const std::vector<nakanawa::char_t> SOKUON_WHITELIST = {
    'b', 't', 'd', 'f', 'g', 'h', 'j', 'k', 'm',
    'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'};

const std::vector<nakanawa::char_t> AMBIGUOUS_VOWELS = {
    L'あ', L'い', L'う', L'え', L'お', L'や', L'ゆ', L'よ'};

namespace helper {

void add_yoon_kana(Node &root) {
  for (auto &x : YOON_KANA) {
    Node &node = root[x];
    // node.

    for (const std::pair<nakanawa::char_t, std::string> &z : SMALL_Y) {
      std::string value = node.get_data()[0] + z.second;
      node.add_child(z.first, value);
    }

    for (const std::pair<nakanawa::char_t, std::string> &z : SMALL_Y_EXTRA) {
      std::string value = node.get_data()[0] + z.second;
      node.add_child(z.first, value);
    }
  }
}

void add_yoon_exceptions(Node &root) {
  for (const auto &x : YOON_EXCEPTIONS) {
    Node &n = root[x.first];

    for (const auto &z : SMALL_Y) {
      n.add_child(z.first, x.second + z.second[1]);
    }
    n.add_child(L'ぃ', x.second + "yi");
    n.add_child(L'ぇ', x.second + "e");
  }
}

void prepend_consonant(Node &node, char c) {
  if (!node.is_empty()) {
    const std::string data = node.get_data();
    node.set_data(c + data);
  }
  if (!node.is_leaf()) {
    std::ranges::for_each(node.get_children(), [&c](auto &pair) {
      prepend_consonant(pair.second, c);
    });
  }
}

void copy_consonants(Node &root, Node &node) {
  auto &node_children = node.get_children();
  for (const auto &x : root.get_children()) {
    nakanawa::char_t c = x.second.get_data()[0];
    if (c == 'c')
      c = 't';
    if (std::ranges::find(SOKUON_WHITELIST, c) != SOKUON_WHITELIST.end()) {
      node_children.push_back(x);
      prepend_consonant(node_children.back().second, c);
    }
  }
}

void add_ambiguos_vowels(Node &root) {
  Node &node = root[L'ん'];
  for (const auto &x : AMBIGUOUS_VOWELS) {
    node.add_child(x, "n'" + root[x].get_data());
  }
}
// for (const auto &x : root.)
} // namespace helper

Node create_kana_to_romanji_map() {
  Node root = transform(BASIC_ROMAJI);
  root.add_children(SPECIAL_SYMBOLS);
  root.add_children(SMALL_Y);
  root.add_children(SMALL_AIUEO);

  helper::add_yoon_kana(root);
  helper::add_yoon_exceptions(root);
  Node &small_tsu = root.add_child(L'っ');
  helper::copy_consonants(root, small_tsu);
  helper::add_ambiguos_vowels(root);

  return root;
}

const Node &get_kana_to_romanji_map() {
  static Node root{create_kana_to_romanji_map()};
  return root;
}
} // namespace nakanawa::utils