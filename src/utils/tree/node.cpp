#include "utils/tree/node.hpp"

namespace nakanawa {
Node &Node::add_child(const std::string &path, const std::string &value) {
  Node &node = find_or_create(path);
  node.set_data(value);
  return node;
}

Node &Node::find_or_create(const std::string &path) {
  Node *current = this;
  for (auto it = path.begin(); it != path.end(); it++) {
    auto found = current->find(*it);
    if (found == current->end()) {
      current = &current->add_child(*it);
    } else {
      current = &found->second;
    }
  }
  return *current;
}
} // namespace nakanawa