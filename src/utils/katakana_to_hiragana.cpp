#include "utils/katakana_to_hiragana.hpp"
#include "constants.hpp"
#include "types.hpp"
#include "utf8/checked.h"
#include "utils/is_char_katakana.hpp"
#include "utils/is_char_long_dash.hpp"
#include "utils/is_char_slash_dot.hpp"
#include <algorithm>

namespace nakanawa::utils {

const std::vector<std::pair<nakanawa::char_t, nakanawa::char_t>> LONG_WOVELS = {
    {L'ァ', L'あ'}, {L'ア', L'あ'}, {L'ィ', L'い'}, {L'イ', L'い'},
    {L'ゥ', L'う'}, {L'ウ', L'う'}, {L'ェ', L'え'}, {L'エ', L'え'},
    {L'ォ', L'お'}, {L'オ', L'お'}, {L'カ', L'あ'}, {L'ガ', L'あ'},
    {L'キ', L'い'}, {L'ギ', L'い'}, {L'ク', L'う'}, {L'グ', L'う'},
    {L'ケ', L'え'}, {L'ゲ', L'え'}, {L'コ', L'お'}, {L'ゴ', L'お'},
    {L'サ', L'あ'}, {L'ザ', L'あ'}, {L'シ', L'い'}, {L'ジ', L'い'},
    {L'ス', L'う'}, {L'ズ', L'う'}, {L'セ', L'え'}, {L'ゼ', L'え'},
    {L'ソ', L'お'}, {L'ゾ', L'お'}, {L'タ', L'あ'}, {L'ダ', L'あ'},
    {L'チ', L'い'}, {L'ヂ', L'い'}, {L'ツ', L'う'}, {L'ヅ', L'う'},
    {L'テ', L'え'}, {L'デ', L'え'}, {L'ト', L'お'}, {L'ド', L'お'},
    {L'ナ', L'あ'}, {L'ニ', L'い'}, {L'ヌ', L'う'}, {L'ネ', L'え'},
    {L'ノ', L'お'}, {L'ハ', L'あ'}, {L'バ', L'あ'}, {L'パ', L'あ'},
    {L'ヒ', L'い'}, {L'ビ', L'い'}, {L'ピ', L'い'}, {L'フ', L'う'},
    {L'ブ', L'う'}, {L'プ', L'う'}, {L'ヘ', L'え'}, {L'ベ', L'え'},
    {L'ペ', L'え'}, {L'ホ', L'お'}, {L'ボ', L'お'}, {L'ポ', L'お'},
    {L'マ', L'あ'}, {L'ミ', L'い'}, {L'ム', L'う'}, {L'メ', L'え'},
    {L'モ', L'お'}, {L'ャ', L'あ'}, {L'ヤ', L'あ'}, {L'ュ', L'う'},
    {L'ユ', L'う'}, {L'ョ', L'お'}, {L'ヨ', L'お'}, {L'ラ', L'あ'},
    {L'リ', L'い'}, {L'ル', L'う'}, {L'レ', L'え'}, {L'ロ', L'お'},
    {L'ワ', L'あ'}, {L'ヰ', L'い'}, {L'ヱ', L'え'}, {L'ヲ', L'お'},
    {L'ヴ', L'う'}};

inline bool is_kana_as_symbol(nakanawa::char_t c) {
  return c == L'ヵ' || c == L'ヶ';
}

std::string katakana_to_hiragana(const std::string &input) {
  std::string result;
  result.reserve(input.size());

  auto inserter = std::back_inserter(result);
  auto it = input.cbegin();
  auto end = input.cend();
  const auto begin = input.cbegin();

  nakanawa::char_t previous_kana = 0;

  while (it != end) {
    nakanawa::char_t c = utf8::next(it, end);

    if (is_char_slash_dot(c) || (begin == it && is_char_long_dash(c)) ||
        is_kana_as_symbol(c)) {
      utf8::append(c, inserter);
    } else if (previous_kana != 0 && is_char_long_dash(c)) {
      auto result = std::lower_bound(
          LONG_WOVELS.cbegin(), LONG_WOVELS.cend(), previous_kana,
          [](const auto &pair, const nakanawa::char_t value) {
            return pair.first < value;
          });
      if (result != LONG_WOVELS.end() && result->first == previous_kana) {
        utf8::append(result->second, inserter);
      }
      previous_kana = 0;
    } else if (!is_char_long_dash(c) && is_char_katakana(c)) {
      auto hiragana =
          c + (constants::HIRAGANA_START - constants::KATAKANA_START);
      utf8::append(hiragana, inserter);
      previous_kana = c;
    } else {
      utf8::append(c, inserter);
    }
  }

  return result;
}
} // namespace nakanawa::utils
