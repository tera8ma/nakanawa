#include "utils/romanji_to_kana_map.hpp"
#include "utils/kana_mapping.hpp"

#include "helpers.hpp"
#include "types.hpp"

#include <algorithm>

namespace nakanawa::utils {
using vec = std::vector<nakanawa::tree_source_node>;
// constants
const nakanawa::tree_source BASIC_KUNREI = {
    {'\0', {{'a', "あ"}, {'i', "い"}, {'u', "う"}, {'e', "え"}, {'o', "お"}}},
    {'k', {{'a', "か"}, {'i', "き"}, {'u', "く"}, {'e', "け"}, {'o', "こ"}}},
    {'s', {{'a', "さ"}, {'i', "し"}, {'u', "す"}, {'e', "せ"}, {'o', "そ"}}},
    {'t', {{'a', "た"}, {'i', "ち"}, {'u', "つ"}, {'e', "て"}, {'o', "と"}}},
    {'n', {{'a', "な"}, {'i', "に"}, {'u', "ぬ"}, {'e', "ね"}, {'o', "の"}}},
    {'h', {{'a', "は"}, {'i', "ひ"}, {'u', "ふ"}, {'e', "へ"}, {'o', "ほ"}}},
    {'m', {{'a', "ま"}, {'i', "み"}, {'u', "む"}, {'e', "め"}, {'o', "も"}}},
    {'y', {{'a', "や"}, {'u', "ゆ"}, {'o', "よ"}}},
    {'r', {{'a', "ら"}, {'i', "り"}, {'u', "る"}, {'e', "れ"}, {'o', "ろ"}}},
    {'w', {{'a', "わ"}, {'i', "ゐ"}, {'e', "ゑ"}, {'o', "を"}}},
    {'g', {{'a', "が"}, {'i', "ぎ"}, {'u', "ぐ"}, {'e', "げ"}, {'o', "ご"}}},
    {'z', {{'a', "ざ"}, {'i', "じ"}, {'u', "ず"}, {'e', "ぜ"}, {'o', "ぞ"}}},
    {'d', {{'a', "だ"}, {'i', "ぢ"}, {'u', "づ"}, {'e', "で"}, {'o', "ど"}}},
    {'b', {{'a', "ば"}, {'i', "び"}, {'u', "ぶ"}, {'e', "べ"}, {'o', "ぼ"}}},
    {'p', {{'a', "ぱ"}, {'i', "ぴ"}, {'u', "ぷ"}, {'e', "ぺ"}, {'o', "ぽ"}}},
    {'v',
     {{'a', "ゔぁ"},
      {'i', "ゔぃ"},
      {'u', "ゔ"},
      {'e', "ゔぇ"},
      {'o', "ゔぉ"}}}};

const std::vector<std::pair<std::string, std::string>> SPECIAL_CASES = {{
    {"yi", "い"},
    {"wu", "う"},
    {"ye", "いぇ"},
    {"wi", "うぃ"},
    {"we", "うぇ"},
    {"kwa", "くぁ"},
    {"whu", "う"},
    // because it's not thya for てゃ but tha
    // and tha is not てぁ, but てゃ
    {"tha", "てゃ"},
    {"thu", "てゅ"},
    {"tho", "てょ"},
    {"dha", "でゃ"},
    {"dhu", "でゅ"},
    {"dho", "でょ"},
}};

const vec CONSONANTS = {{
    {'k', "き"},
    {'s', "し"},
    {'t', "ち"},
    {'n', "に"},
    {'h', "ひ"},
    {'m', "み"},
    {'r', "り"},
    {'g', "ぎ"},
    {'z', "じ"},
    {'d', "ぢ"},
    {'b', "び"},
    {'p', "ぴ"},
    {'v', "ゔ"},
    {'q', "く"},
    {'f', "ふ"},
}};

const std::vector<std::pair<std::string, std::string>> SMALL_Y = {
    {{"ya", "ゃ"}, {"yi", "ぃ"}, {"yu", "ゅ"}, {"ye", "ぇ"}, {"yo", "ょ"}}};

const vec SPECIAL_SYMBOLS = {{
    {L'.', "。"},
    {L',', "、"},
    {L':', "："},
    {L'/', "・"},
    {L'!', "！"},
    {L'?', "？"},
    {L'~', "〜"},
    {L'-', "ー"},
    {L'‘', "「"},
    {L'’', "」"},
    {L'“', "『"},
    {L'”', "』"},
    {L'[', "［"},
    {L']', "］"},
    {L'(', "（"},
    {L')', "）"},
    {L'{', "｛"},
    {L'}', "｝"},
}};

const std::vector<std::pair<std::string, std::string>> AIUEO_CONSTRUCTIONS = {{
    {"wh", "う"},
    {"qw", "く"},
    {"q", "く"},
    {"gw", "ぐ"},
    {"sw", "す"},
    {"ts", "つ"},
    {"th", "て"},
    {"tw", "と"},
    {"dh", "で"},
    {"dw", "ど"},
    {"fw", "ふ"},
    {"f", "ふ"},
}};

const std::vector<std::pair<std::string, std::string>> SMALL_VOWELS = {
    {{"a", "ぁ"}, {"i", "ぃ"}, {"u", "ぅ"}, {"e", "ぇ"}, {"o", "ぉ"}}};

const std::vector<std::pair<std::string, std::string>> ALIASES = {{
    {"sh", "sy"},  // sha -> sya
    {"ch", "ty"},  // cho -> tyo
    {"cy", "ty"},  // cyo -> tyo
    {"chy", "ty"}, // chyu -> tyu
    {"shy", "sy"}, // shya -> sya
    {"j", "zy"},   // ja -> zya
    {"jy", "zy"},  // jye -> zye

    // exceptions to above rules
    {"shi", "si"},
    {"chi", "ti"},
    {"tsu", "tu"},
    {"ji", "zi"},
    {"fu", "hu"},
}};

const std::vector<std::pair<std::string, std::string>> SMALL_LETTERS = {
    {{"tu", "っ"},
     {"wa", "ゎ"},
     {"ka", "ヵ"},
     {"ke", "ヶ"},
     // SMALL_VOWELS
     {"a", "ぁ"},
     {"i", "ぃ"},
     {"u", "ぅ"},
     {"e", "ぇ"},
     {"o", "ぉ"},
     // SMALL_Y
     {"ya", "ゃ"},
     {"yi", "ぃ"},
     {"yu", "ゅ"},
     {"ye", "ぇ"},
     {"yo", "ょ"}}};

// end constants
namespace helpers {
void create_additional_nodes(Node &root, const auto &paths,
                             const auto &additional) {
  std::ranges::for_each(paths, [&root, &additional](const auto &consonant) {
    auto &[c, syllable] = consonant;
    auto &node = root.find_or_create(c);
    std::ranges::for_each(
        additional, [&node, syllable = std::move(syllable)](const auto &y) {
          const std::string &path = std::get<0>(y);
          std::string value = syllable + std::get<1>(y);
          node.add_child(path, value);
        });
  });
}

void add_different_n(Node &root) {
  std::string ns[] = {"n", "n'", "xn"};
  std::ranges::for_each(ns, [&root](const auto &path) {
    std::string n = "ん";
    root.add_child(path, n);
  });
}

void add_aliases(Node &root) {
  auto &cNode = root.add_child('c');
  cNode = root['k'];

  std::ranges::for_each(ALIASES, [&root](const auto &copyToFrom) {
    const auto &[to, from] = copyToFrom;
    auto &copyToNode = root.find_or_create(to);
    copyToNode = root.traverse(from);
  });
}

void add_small_letters(Node &root) {
  for (const auto &letters : SMALL_LETTERS) {
    const auto &kunreiRoma = letters.first;
    const auto &kana = letters.second;

    const std::string xRoma{"x" + kunreiRoma};
    const Node &xSubtree = root.add_child(xRoma, kana);

    const std::string lRoma{"l" + kunreiRoma};
    Node &parentTree = root.find_or_create(lRoma);
    parentTree = xSubtree;

    auto extendedAliases = ALIASES;
    extendedAliases.emplace_back("c", "k");

    auto it =
        std::ranges::find_if(extendedAliases, [&kunreiRoma](const auto &x) {
          return kunreiRoma.starts_with(std::get<1>(x));
        });

    if (it != extendedAliases.end()) {
      const std::string &begin = std::get<0>(*it);
      const std::string &start = std::get<1>(*it);
      std::string prefixes[] = {"l", "x"};
      std::string altRoma{
          begin +
          kunreiRoma.substr(start.size(), kunreiRoma.size() - start.size())};
      for (auto x : prefixes) {
        Node &altParentTree = root.find_or_create(x + altRoma);
        altParentTree = root.traverse(x + kunreiRoma);
      }
    }
  }
}

void apply_special_cases(Node &root) {
  for (const auto &p : SPECIAL_CASES) {
    root.find_or_create(p.first).set_data(p.second);
  }
}

void add_tsu_subprocess(Node &node) {
  if (!node.get_data().empty()) {
    node.set_data("っ" + node.get_data());
  }
  auto &children = node.get_children();
  if (!children.empty()) {
    std::ranges::for_each(
        children, [](auto &pair) { add_tsu_subprocess(std::get<1>(pair)); });
  }
}

void add_tsu(Node &root) {
  std::vector<nakanawa::char_t> keys;
  keys.reserve(CONSONANTS.size() + 4);
  for (const auto &pair : CONSONANTS) {
    auto c = std::get<0>(pair);
    if (c != 'n') {
      keys.push_back(c);
    }
  }

  keys.push_back(static_cast<nakanawa::char_t>('c'));
  keys.push_back(static_cast<nakanawa::char_t>('y'));
  keys.push_back(static_cast<nakanawa::char_t>('w'));
  keys.push_back(static_cast<nakanawa::char_t>('j'));

  for (const auto &x : keys) {
    Node node;
    node = root[x];
    add_tsu_subprocess(node);
    std::string s;
    s.push_back(x);
    s.push_back(x);
    Node &newNode = root.add_child(s, s);
    newNode = node;
  }
}
} // namespace helpers
Node create_romaji_to_kana_map() {
  Node root = transform(BASIC_KUNREI);

  // add tya, sya, etc.
  helpers::create_additional_nodes(root, CONSONANTS, SMALL_Y);
  // things like うぃ, くぃ, etc.
  helpers::create_additional_nodes(root, AIUEO_CONSTRUCTIONS, SMALL_VOWELS);
  // different ways to write ん
  helpers::add_different_n(root);
  // c is equivalent to k, but not for chi, cha, etc. that's why we have to make
  // a copy of k
  helpers::add_aliases(root);

  helpers::add_small_letters(root);

  helpers::apply_special_cases(root);

  root.add_children(SPECIAL_SYMBOLS);

  helpers::add_tsu(root);
  return root;
}

Node create_romaji_ime_map() {
  Node root = create_romaji_to_kana_map();

  root.add_child("nn", "ん");
  root.add_child("n ", "ん");

  return root;
}

const Node &get_romanji_ime_map() {
  static Node root{create_romaji_ime_map()};
  return root;
}

const Node &get_romaji_map() {
  static Node root{create_romaji_to_kana_map()};
  return root;
}

}; // namespace nakanawa::utils