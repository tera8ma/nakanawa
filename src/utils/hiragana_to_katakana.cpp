#include "utils/hiragana_to_katakana.hpp"
#include "constants.hpp"
#include "utf8.h"
#include "utf8/checked.h"
#include "utils/is_char_hiragana.hpp"
#include "utils/is_char_long_dash.hpp"
#include "utils/is_char_slash_dot.hpp"

namespace nakanawa::utils {
std::string hiragana_to_katakana(std::string::const_iterator begin,
                                 std::string::const_iterator end) {
  std::string result;
  result.reserve(std::distance(begin, end));

  auto inserter = std::back_inserter(result);

  while (begin != end) {
    auto c = utf8::next(begin, end);
    if (is_char_long_dash(c) || is_char_slash_dot(c)) {
      utf8::append(c, inserter);
    } else if (is_char_hiragana(c)) {
      auto kana_char =
          c - constants::HIRAGANA_START + constants::KATAKANA_START;
      utf8::append(kana_char, inserter);
    } else {
      utf8::append(c, inserter);
    }
  }
  return result;
}
} // namespace nakanawa::utils