#include "utils/kana_mapping.hpp"
#include "utf8/checked.h"
#include <string>
#include <utf8.h>

namespace nakanawa::utils {
Node transform(const nakanawa::tree_source &map) {
  Node root;

  for (const auto &row : map) {
    if (row.first == '\0') {
      root.add_children(row.second);
    } else {
      Node &n = root.add_child(row.first);
      n.add_children(row.second);
    }
  }

  return root;
}

Chunk new_chunk(const Node &root, std::string::const_iterator begin,
                std::string::const_iterator end, bool ime_mode) {
  auto it = begin;
  auto last = begin;
  const Node *node = &root;
  while (it != end) {
    auto c = std::tolower(utf8::peek_next(it, end));

    auto next_node = node->find(c);
    if (next_node == node->end()) {
      if (!node->is_empty()) {
        return Chunk{begin, it, node->get_data()};
      } else if (node == &root) {
        std::string value;
        utf8::append(c, std::back_inserter(value));
        utf8::advance(it, 1, end);

        return Chunk{begin, it, begin, it};
      }
      node = &root;
    } else {
      node = &next_node->second;
      utf8::advance(it, 1, end);
      if (node->is_leaf()) {
        return Chunk{begin, it, node->get_data()};
      }
    }
  }
  if (node != &root &&
      (ime_mode && !node->is_leaf() || !ime_mode && node->is_empty())) {
    return Chunk{begin, it, begin, it};
  }

  return Chunk{begin, it, node->get_data()};
}

} // namespace nakanawa::utils