#include <catch.hpp>
#include <string>

#include "helpers/conversion_tables.hpp"
#include "romanji_converter.hpp"
#include "types.hpp"
#include "utils/romanji_to_kana_map.hpp"

using namespace nakanawa;

TEST_CASE("create_romaji_to_kana_map()") {
  const Node root{utils::create_romaji_to_kana_map()};
  RomanjiConverter r2k(false);

  SECTION("Lowercase characters are transliterated to hiragana") {
    REQUIRE(r2k.convert("onaji") == "おなじ");
  }

  SECTION("Lowercase with double consonants and double vowels are "
          "transliterated to hiragana") {
    REQUIRE(r2k.convert("buttsuuji") == "ぶっつうじ");
  }

  SECTION("Uppercase characters are transliterated to katakana") {
    REQUIRE(r2k.convert("ONAJI") == "オナジ");
  }

  SECTION("Uppercase with double consonants and double vowels are "
          "transliterated to katakana") {
    REQUIRE(r2k.convert("BUTTSUUJI") == "ブッツウジ");
  }

  SECTION("WaniKani -> わにかに - Mixed case returns hiragana (katakana only "
          "if all letters of mora are uppercased") {
    REQUIRE(r2k.convert("WaniKani") == "わにかに");
  }

  SECTION("Non-romaji will be passed through") {
    REQUIRE(r2k.convert("ワニカニ AiUeO 鰐蟹 12345 @#$%") ==
            "ワニカニ アいウえオ 鰐蟹 12345 @#$%");
  }

  SECTION("It handles mixed syllabaries") {
    const std::string result = r2k.convert("座禅‘zazen’スタイル");
    REQUIRE(result == "座禅「ざぜん」スタイル");
  }

  SECTION("Will convert short to long dashes") {
    REQUIRE(r2k.convert("batsuge-mu") == "ばつげーむ");
  }

  SECTION("Will convert punctuation but pass through spaces") {
    REQUIRE(r2k.convert(join(EN_PUNC, ' ')) == join(JA_PUNC, ' '));
  }

  SECTION("without IME Mode") {
    SECTION("solo n's are transliterated regardless of following chars") {
      REQUIRE(r2k.convert("n") == "ん");
      REQUIRE(r2k.convert("shin") == "しん");
    }
    REQUIRE(r2k.convert("nn") == "んん");
  }

  SECTION("with IME Mode") {
    RomanjiConverter r2k_ime(true);
    SECTION("solo n's are not transliterated unless chars follow") {
      REQUIRE(r2k_ime.convert("n") == "n");
      REQUIRE(r2k_ime.convert("shin") == "しn");
      REQUIRE(r2k_ime.convert("shinyou") == "しにょう");
      REQUIRE(r2k_ime.convert("shin'you") == "しんよう");
      REQUIRE(r2k_ime.convert("shin you") == "しんよう");
    }
    REQUIRE(r2k_ime.convert("nn") == "ん");
  }

  SECTION("Quick Brown Fox - Romaji to Katakana") {
    REQUIRE(r2k.convert("IROHANIHOHETO", true) == "イロハニホヘト");
    // die sooner or later.'
    REQUIRE(r2k.convert("CHIRINURUWO", true) == "チリヌルヲ"); // ちりぬるを
    // Us who live in this world'
    REQUIRE(r2k.convert("WAKAYOTARESO", true) ==
            "ワカヨタレソ"); // わかよたれそ
    // cannot live forever, either.'
    REQUIRE(r2k.convert("TSUNENARAMU", true) == "ツネナラム"); // つねならむ
    // today we are going to overcome, and reach the world of enlightenment.'
    REQUIRE(r2k.convert("KEFUKOETE", true) == "ケフコエテ"); // けふこえて
    // We are not going to have meaningless dreams'
    REQUIRE(r2k.convert("ASAKIYUMEMISHI", true) ==
            "アサキユメミシ"); // あさきゆめみし
    // *not in iroha*
    REQUIRE(r2k.convert("NLTU") == "ンッ"); // んっ
  }
}