#pragma once
#include "types.hpp"
#include <array>
#include <string>
#include <utf8.h>

constexpr const std::array<nakanawa::char_t, 18> JA_PUNC = {
    L'！', L'？', L'。', L'：', L'・', L'、', L'〜', L'ー', L'「',
    L'」', L'『', L'』', L'［', L'］', L'（', L'）', L'｛', L'｝'};

constexpr const std::array<nakanawa::char_t, 18> EN_PUNC = {
    L'!', L'?', L'.', L':', L'/', L',', L'~', L'-', L'‘',
    L'’', L'“', L'”', L'[', L']', L'(', L')', L'{', L'}'};

template <std::size_t size>
std::string join(const std::array<nakanawa::char_t, size> &array, char delim) {
  std::string result;
  result.reserve(size * 4);
  auto inserter = std::back_inserter(result);

  for (const auto &x : array) {
    utf8::append(x, inserter);
    utf8::append(delim, inserter);
  }

  result.shrink_to_fit();
  return result;
}