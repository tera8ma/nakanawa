#include "helpers/conversion_tables.hpp"
#include "kana_converter.hpp"
#include "utils/kana_to_romanji_map.hpp"
#include <catch.hpp>
#include <string>

using namespace nakanawa;

TEST_CASE("create_romaji_to_kana_map()") {
  KanaConverter k2r;

  SECTION("Convert katakana to romaji'") {
    REQUIRE(k2r.convert("ワニカニ　ガ　スゴイ　ダ") == "wanikani ga sugoi da");
  }

  SECTION("Convert hiragana to romaji") {
    REQUIRE(k2r.convert("わにかに　が　すごい　だ") == "wanikani ga sugoi da");
  }

  SECTION("Convert mixed kana to romaji") {
    REQUIRE(k2r.convert("ワニカニ　が　すごい　だ") == "wanikani ga sugoi da");
  }

  SECTION("Will convert punctuation and full-width spaces") {
    REQUIRE(k2r.convert(join(JA_PUNC, ' ')) == k2r.convert(join(EN_PUNC, ' ')));
  }

  SECTION("Use the upcaseKatakana flag to preserve casing. "
          "Works for katakana") {
    REQUIRE(k2r.convert("ワニカニ", true) == "WANIKANI");
  }

  SECTION("Use the upcaseKatakana flag to preserve casing. "
          "Works for mixed kana") {
    REQUIRE(k2r.convert("ワニカニ　が　すごい　だ", true) ==
            "WANIKANI ga sugoi da");
  }

  SECTION("Converts long dash 'ー' in hiragana to hyphen") {
    REQUIRE(k2r.convert("ばつげーむ") == "batsuge-mu");
  }

  SECTION("Doesn't confuse '一' (one kanji) for long dash 'ー'") {
    REQUIRE(k2r.convert("一抹げーむ") == "一抹ge-mu");
  }

  SECTION("Converts long dash 'ー' (chōonpu) in katakana to long vowel") {
    REQUIRE(k2r.convert("スーパー") == "suupaa");
  }

  SECTION("Doesn't convert オー to 'ou' which occurs with hiragana") {
    REQUIRE(k2r.convert("缶コーヒー") == "缶koohii");
  }

  SECTION("Spaces must be manually entered") {
    REQUIRE(k2r.convert("わにかにがすごいだ") != "wanikani ga sugoi da");
  }

  SECTION("double n's and double consonants") {
    REQUIRE(k2r.convert("きんにくまん") == "kinnikuman");
    REQUIRE(k2r.convert("んんにんにんにゃんやん") == "nnninninnyan'yan");
    REQUIRE(k2r.convert("かっぱ　たった　しゅっしゅ ちゃっちゃ　やっつ") ==
            "kappa tatta shusshu chatcha yattsu");
  }

  SECTION("Small kana") {
    REQUIRE(k2r.convert("ヶ") == "ヶ");
    REQUIRE(k2r.convert("ヵ") == "ヵ");
    REQUIRE(k2r.convert("ゃ") == "ya");
    REQUIRE(k2r.convert("ゅ") == "yu");
    REQUIRE(k2r.convert("ょ") == "yo");
    REQUIRE(k2r.convert("ぁ") == "a");
    REQUIRE(k2r.convert("ぃ") == "i");
    REQUIRE(k2r.convert("ぅ") == "u");
    REQUIRE(k2r.convert("ぇ") == "e");
    REQUIRE(k2r.convert("ぉ") == "o");
  }

  SECTION("Apostrophes in ambiguous consonant vowel combos") {
    REQUIRE(k2r.convert("おんよみ") == "on'yomi");
    REQUIRE(k2r.convert("んよ んあ んゆ") == "n'yo n'a n'yu");
    REQUIRE(k2r.convert("シンヨ") == "shin'yo");
  }
}