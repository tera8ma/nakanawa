cmake_minimum_required(VERSION 3.8)
# Project name
project(nakanawa)

set (CMAKE_CXX_STANDARD 20)

add_subdirectory(third_party/Catch)
include_directories(third_party/Catch/include)

add_subdirectory(third_party/utfcpp)
include_directories(third_party/utfcpp/source)

include_directories(include)
add_subdirectory(test)
add_subdirectory(src)